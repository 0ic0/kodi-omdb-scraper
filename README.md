#### A Kodi metadata plugin using the OMDB API
----
#### Requirements
* Kodi > v.18 - Kodi has support for Pyhton based scrapers in version 18. The add-on will not function on lower versions even if it's forcefully installed.

##### Usage
* Install the add-on. Use the latest ZIP from [Releases](https://gitlab.com/0ic0/kodi-omdb-scraper/releases)
* From the add-on settings screen:
  * Enter your [API Key](http://omdbapi.com/apikey.aspx)
  * Choose a ratings provider IMDB (Default) / Rotten Tomatoes
* Set the OMDB Scraper as content provider for your Movies collection
* Scan your library & Enjoy :-)


##### Build
* Clone the repo
* Modify the code which you want to improve
* Use the zip_prepare.py - it will create a ZIP file which can be installed on Kodi




