import xbmcplugin
import xbmcgui
import xbmc
import xbmcaddon
import sys
from .omdb_base.search import Movie
try:
    from urllib.parse import parse_qsl
except ImportError:
    from urlparse import parse_qsl

ADDON = xbmcaddon.Addon()
ID = ADDON.getAddonInfo('id')
LANGUAGE = ADDON.getSetting('language')
API_KEY = ADDON.getSetting('api_key')
RATINGS_PROVIDER = int(ADDON.getSetting('provider'))
HANDLE = int(sys.argv[1])

def log(msg):
    xbmc.log(msg='{addon}: {msg}'.format(addon=ID, msg=msg), level=xbmc.LOGDEBUG)


def get_search_args():
    if not sys.argv[2]:
        log('Error. No arguments provided!')
        return {}
    else:
        return dict(parse_qsl(sys.argv[2].lstrip('?')))


def add_movie_id(title, **kwargs):
    """Extract the imdbID. and add it to the XBMC database"""
    movie = Movie(API_KEY)
    result = movie.search(title, kwargs.get('year', None))
    if isinstance(result, dict):
        item = xbmcgui.ListItem(result['Title'], offscreen=True)
        xbmcplugin.addDirectoryItem(handle=HANDLE, url=str(result['imdbID']), listitem=item, isFolder=True)
    else:
        log('Error {err}'.format(err=result))


def get_movie_details(url):
    movie = Movie(API_KEY)
    res = movie.kodi_data(url)
    if len(res) == 0:
        log('Response length is 0. Check your url')
        return res
    if ADDON.getSetting('keeporiginaltitle') == 'true':
        title = res['originaltitle']
    else:
        title = res['Title']
    _xbmc = xbmcgui.ListItem(title, offscreen=True)
    _xbmc.setInfo('video',
                  {
                      'title': title,
                      'originaltitle': res['originaltitle'],
                      'genre': res['Genre'].split(',') if len(res['Genre'].split(',')) > 1 else res['Genre'],
                      'plot': res['Plot'],
                      'duration': res['Runtime'],
                      'year': res['Year'],
                      'mpaa': res['Rated'],
                      'premiered': res['Released'],
                      'country': res['Country'].split(',') if len(res['Country'].split(',')) > 1 else res['Country']
                  }
                  )
    if res.get('Poster', None):
        _xbmc.addAvailableArtwork(res['Poster'])
    log('Will set UniqueID: {}'.format(res['imdbID']))
    _xbmc.setUniqueIDs({'imdb': res['imdbID']}, 'imdb')
    log('Set Ratings for provider: {}, chosen by due {}'.format(RATINGS_PROVIDER, ADDON.getSetting('provider')))
    if res.get('imdb_rating', None):
        log('Will set IMDB rating to: {}'.format(res['imdb_rating']))
        if RATINGS_PROVIDER == 1:
            _xbmc.setRating('imdb', res['imdb_rating'])
        else:
            # Set a default provider
            _xbmc.setRating('imdb', res['imdb_rating'], int(res['imdbVotes']), True)
    if res.get('rotten_rating', None):
        log('Will set Rotten Tomatoes rating to: {}'.format(res['rotten_rating']))
        if RATINGS_PROVIDER == 0:
            _xbmc.setRating('rotten', res['rotten_rating'])
        else:
            _xbmc.setRating('rotten', res['rotten_rating'], 0, True)
    if res.get('Actors', None):
        log('Adding actors: {}'.format(res['Actors']))
        _xbmc.setCast(res['Actors'])

    xbmcplugin.setResolvedUrl(handle=HANDLE, succeeded=True, listitem=_xbmc)


def run():
    log('Called with args [{handle} {args}]'.format(handle=HANDLE, args=sys.argv[2]))

    args = get_search_args()
    action = args.get('action', None)
    if action:
        if action == 'find' and 'title' in args:
            add_movie_id(args['title'], year=args.get('year', None))
        elif action == 'getdetails' and 'url' in args:
            get_movie_details(args['url'])

    xbmcplugin.endOfDirectory(HANDLE)
