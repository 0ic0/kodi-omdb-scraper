import requests
import json
try:
    from urllib.parse import quote
except ImportError:
    from urllib import quote


class APIKeyError(Exception):
    pass


class OMDB(object):

    headers = {
        'Content-Type': 'application/json',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Application': 'application/json',
        'Connection': 'close',
        'User-Agent': 'OMDB Search/0.1'
    }

    def __init__(self):
        self.base_url = 'https://www.omdbapi.com/?'

    def _construct_url(self, key, attrib):

        url = self.base_url+'apikey='+key
        if attrib.get('search', None):
            url += '&s=' + quote(attrib['search'])
            return url
        if attrib.get('title', None):
            url += '&t=' + quote(attrib['title'])
        if attrib.get('id', None):
            url += '&i=' + quote(attrib['id'])
        if attrib.get('season', None):
            url += '&season=' + quote(attrib['season'])
        if attrib.get('episode', None):
            url += '&episode=' + quote((attrib['episode']))
        if attrib.get('year', None):
            url += '&year=' + quote((attrib['year']))
        return url

    def _request(self, api_key, query):
        url = self._construct_url(api_key, query)
        resp = requests.request('GET', url, headers=self.headers, )
        if resp.status_code == 200:
            return resp.json()
        else:
            return json.dumps('{}')
