from .base import OMDB
import json
import xbmc
import xbmcaddon

ADDON = xbmcaddon.Addon()
ID = ADDON.getAddonInfo('id')


class Search(OMDB):

    def __init__(self, api_key):
        super(Search, self).__init__()
        self.auth = api_key

    def log(self, msg):
        xbmc.log(msg='{addon}: {msg}'.format(addon=ID, msg=msg), level=xbmc.LOGDEBUG)

    def by_name(self, name, year=None):
        """Search by title"""
        query = {'title': name}
        if year:
            query['year'] = year
        return self._search(query)

    def by_id(self, imdb_id):
        """Search by IMDB ID"""
        query = {'id': imdb_id}
        return self._search(query)

    def _search(self, query):
        data = self._request(self.auth, query)
        if data['Response'].startswith('True'):
            if data['Type'].startswith('series') or data['Type'].startswith('movie'):
                return data
            else:
                return 'Unknown data type!'
        else:
            if data.get('Error', None):
                return data['Error']

    def generic(self, name):
        query = {'search': name}
        return self._search(query)


class Show(Search):
    def __init__(self, api_key, name):
        super(Show, self).__init__(api_key)
        self.name = name

    def seasons(self):
        pass

    def episodes(self):
        pass

    def episode_info(self):
        pass


class Movie(Search):
    kodi_meta = {
        'Title': None,
        'originaltitle': None,
        'Genre': None,
        'Year': None,
        'Runtime': None,
        'Language': None,
        'Director': None,
        'Poster': None,
        'Plot': None,
        'Rated': None,
        'imdbID': None,
        'Released': None,
        'Country': None,
        'imdbVotes': None
    }

    def __init__(self, api_key):
        super(Movie, self).__init__(api_key)

    def search(self, name, year=None):
        result = self.by_name(name, year)
        return result

    def kodi_data(self, imdb):
        s = self.by_id(imdb)
        if isinstance(s, str) or isinstance(s, unicode):
            return json.loads('{}')
        for i in self.kodi_meta.keys():
            if s.get(i, None):
                self.kodi_meta[i] = s[i]
        # Find a request for a movie with name in different languages and see how to set the original title
        # Until then set the originaltite from title
        self.kodi_meta['originaltitle'] = self.kodi_meta['Title']
        # Build a KODI compatible list with the actors
        if s.get('Actors', None):
            self.kodi_meta['Actors'] = []
            for u in s['Actors'].split(','):
                self.kodi_meta['Actors'].append({'name': u})
        # Build a list with the ratings of different services
        if s.get('Ratings', None):
            for i in range(0, len(s['Ratings'])):
                self.__rating(s['Ratings'][i])
        # Fix for IMDB votes where ',' is used as a separator
        # isinstance unicode check needed for Python 2
        if isinstance(self.kodi_meta['imdbVotes'], str) or isinstance(self.kodi_meta['imdbVotes'], unicode):
            self.kodi_meta['imdbVotes'] = self.kodi_meta['imdbVotes'].replace(',', '')
        return self.kodi_meta

    def __rating(self, x):
        self.log('Trying to set rating: {}'.format(x))
        if isinstance(x, dict):
            if x["Source"] == "Internet Movie Database":
                self.kodi_meta['imdb_rating'] = float(x['Value'].split('/')[0])
                self.log('IMDB rating evaluated as: {}'.format(self.kodi_meta['imdb_rating']))
            elif x["Source"] == "Rotten Tomatoes":
                self.kodi_meta['rotten_rating'] = float(x['Value'].split('%')[0])/10
                self.log('Rotten rating evaluated as: {}'.format(self.kodi_meta['rotten_rating']))
            else:
                pass


