import shutil
import os

target_dir = 'metadata.api_omdbapi.com'
res_dir = target_dir+'/resources'

# Cleanup - remove and recreate the target dir if exists
if os.path.exists(target_dir):
    shutil.rmtree(target_dir)

os.mkdir(target_dir)
shutil.copy('LICENSE', target_dir)
shutil.copy('addon.xml', target_dir)
shutil.copy('scraper.py', target_dir)
shutil.copytree('resources', res_dir, ignore=shutil.ignore_patterns('__pycache__'))
shutil.make_archive('metadata.omdbapi_kodi', 'zip', '.', target_dir, verbose=True)
